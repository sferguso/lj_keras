#!/usr/bin/env python3
import random
import argparse
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import h5py
import os
import json
import uproot
import math
import keras
from keras.utils import to_categorical
import sklearn
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
from keras import backend as K

def get_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('sig_train')
    parser.add_argument('bg_train')
    parser.add_argument('sig_validation')
    parser.add_argument('bg_validation')
    #LJ type: 7-eLJ, 6-mixLJ, 5-muLJ (order does not matter)
    parser.add_argument('--LJ1_type', type=int, default=7)
    parser.add_argument('--LJ2_type', type=int, default=7)
    parser.add_argument('-e','--epochs', type=int, default=1)
    parser.add_argument('-p', '--patience', type=int, default=1) #patience for early stopping
    parser.add_argument('-o','--output-dir', default='model')
    return parser.parse_args()

def precision_metric(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision

def recall_metric(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall

def downsample(df_true, df_false):
    initial_len = [len(df_true), len(df_false)]
    sample_len = min(initial_len)
    print('downsampling')
    df_true = df_true.sample(sample_len)
    df_false = df_false.sample(sample_len)

    # Combine and shuffle the true and false dataframes
    return df_true.append(df_false).sample(frac=1)

def rescale(df, dict, list_of_vars):
    for var, (min,max) in dict.items():
        # Want to normalize each variable
        data = df[var]
        normalized = (data - min)/(max-min)
        df[var]=normalized
    x = df[list_of_vars]
    y = df['targets_LJ']
    return (np.array(x), np.array(y))

###############################################################
# Main Training Part
###############################################################

def run():
    print("start")
    args = get_args()

    #open training file
    sig_train_file = args.sig_train
    bg_train_file = args.bg_train
    #open validation file
    sig_val_file = args.sig_validation
    bg_val_file = args.bg_validation

    print('list of filepaths for training data: ' + sig_train_file + ', ' + bg_train_file)
    print('list of filepaths for validation data: ' + sig_val_file + ', ' + bg_val_file)

    list_of_vars = [
        'part_LJindex','part_objNum','pdgID','nLJ7','nLJ6','nLJ5',
#        'isMatched','PartToTrig', 'hasPV','trigMatch','trigxAODMatch',
        'part_pt','part_phi','part_eta',
        'el_f1','el_f3','el_f3core','el_e255',
#        'el_e277',
        'el_weta1','el_weta2','el_fracs1','el_widths1',
        'el_widths2','el_poscs1','el_poscs2','el_asy1','el_pos','el_pos7','el_barys1',
#        'el_wtots1',
        'el_r33over37allcalo','el_ecore','el_Reta','el_Rphi','el_Eratio','el_Rhad','el_Rhad1','el_DeltaE',
        'calo_energyBE0','calo_energyBE1','calo_energyBE2','calo_energyBE3',
        'calo_rawE','calo_E',
        'el_nTrackParticles',
        'el_deltaEta0','el_deltaEta1','el_deltaEta2','el_deltaEta3',
        'el_deltaPhi0','el_deltaPhi1','el_deltaPhi2','el_deltaPhi3',
        'el_deltaPhiRescaled0','el_deltaPhiRescaled1','el_deltaPhiRescaled2','el_deltaPhiRescaled3',
        'el_deltaPhiFromLastMeasurement',
        'iso_ptcone20','iso_ptcone30','iso_ptcone40','iso_ptvarcone20','iso_ptvarcone30','iso_ptvarcone40',
        'iso_etcone20','iso_etcone30','iso_etcone40','iso_ettopocone20','iso_ettopocone30','iso_ettopocone40',
        'LJ_pT'
        ]

    print("list of vars: " + str(list_of_vars))

    rescale_dict = { #varname: (min, max)
            'part_pt': (0,200),'part_phi': (-3.5,3.5),'part_eta': (-2.5,2.5),
            'el_e255': (0,300000),
#            'el_e277': (0,300000),
            'el_weta2': (-0.01,0.04),
            'el_pos7': (-6,6),'el_barys1': (-3,3),
#            'el_wtots1': (0,10),
            'el_ecore': (0,300000),'el_DeltaE': (0,2000),
            'calo_energyBE0': (0,40000),'calo_energyBE1': (0,125000),'calo_energyBE2': (0,200000),'calo_energyBE3': (0,25000),
            'calo_rawE': (0,400000),'calo_E': (0,400000),
            'el_deltaEta0': (-0.1,0.1),'el_deltaEta1': (-0.1,0.1),'el_deltaEta2': (-0.1,0.1),'el_deltaEta3': (-0.1,0.1),
            'el_deltaPhi0': (-0.75,0.25),'el_deltaPhi1': (-0.75,0.25),'el_deltaPhi2': (-0.75,0.25),'el_deltaPhi3': (-0.75,0.25),
            'el_deltaPhiRescaled0': (-0.25,0.25),'el_deltaPhiRescaled1': (-0.25,0.25),
            'el_deltaPhiRescaled2': (-0.25,0.25),'el_deltaPhiRescaled3': (-0.25,0.25),
            'el_deltaPhiFromLastMeasurement': (-0.75,0.25),
            'iso_ptcone20': (0,300000),'iso_ptcone30': (0,300000),'iso_ptcone40': (0,300000),
            'iso_ptvarcone20': (0,300000),'iso_ptvarcone30': (0,300000),'iso_ptvarcone40': (0,300000),
            'iso_etcone20': (0,300000),'iso_etcone30': (0,300000),'iso_etcone40': (0,300000),
            'iso_ettopocone20': (0,300000),'iso_ettopocone30': (0,300000),'iso_ettopocone40': (0,300000),
            'LJ_pT': (0,200000)
            }

    #open hdf5 files: 
    sig_train = pd.read_hdf(sig_train_file, "sig")
    bg_train = pd.read_hdf(bg_train_file, "bg")
    sig_val = pd.read_hdf(sig_val_file, "sig")
    bg_val = pd.read_hdf(bg_val_file, "bg")

    #check that sig/bg is 50/50 of train and validation sets: 
    print('before downsizing: ')
    print('length of sig_train: ' + str(len(sig_train)))
    print('length of bg_train:	' + str(len(bg_train)))
    print('length of sig_val: ' + str(len(sig_val)))
    print('length of bg_val:  ' + str(len(bg_val)))
    train_df = downsample(sig_train, bg_train)
    val_df = downsample(sig_val, bg_val)
    print('after downsampling: ')
    print('length of train_df: ' + str(len(train_df)))
    print('length of val_df: ' + str(len(val_df)))

    (x_train, y_train) = rescale(train_df, rescale_dict, list_of_vars)
    (x_val, y_val) = rescale(val_df, rescale_dict, list_of_vars)

    #lets do a sequential model
    from keras.models import Sequential
    from keras.layers import Dense, Activation

    print('using relu activation function')
    model = Sequential()
    model.add(Dense(len(list_of_vars), input_dim=len(list_of_vars), activation='relu'))
    model.add(Dense(len(list_of_vars)*2, activation='relu'))
    model.add(Dense(len(list_of_vars), activation='relu'))
    #sigmoid is recommended for binary classification with binary_crossentropy, for multiclasses use softmax with categorical_crossentropy
    model.add(Dense(1, activation='sigmoid')) 

    #print model summary
    model.summary()
    
    # compile the keras model 
    model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['binary_accuracy', precision_metric, recall_metric])

    #create early stopping for if the nn is not improving
    from keras.callbacks import EarlyStopping
    early_stop = EarlyStopping(monitor='val_loss', mode='max', patience=args.patience, restore_best_weights=True)

    #fit model
    history = model.fit(x_train, y_train, epochs=args.epochs, batch_size=1000, callbacks=[early_stop], validation_data = (x_val, y_val))


    #Confusion matrix
    LJ_prob = model.predict(x_val, batch_size=1000)
    LJ_pred = LJ_prob > 0.5

    print('Confusion Matrix')
    print(confusion_matrix(y_val,LJ_pred))
    tn, fp, fn, tp = confusion_matrix(y_val,LJ_pred).ravel()
    print('true positive: ' + str(tp))
    print('false positive: ' + str(fp))
    print('false negative: ' + str(fn))
    print('true negative: ' + str(tn))
    precision = tp/(tp + fp)
    recall = tp/(tp + fn)
    F1score = 2*(precision * recall)/(precision + recall)
    print('precision: ' + str(precision))
    print('recall: ' + str(recall))
    print('F1score: ' + str(F1score))

    #plot validation loss vs epochs
    print("history: " + str(history.history) + " type: " + str(type(history.history)))
    print("val_loss: " + str(history.history['val_loss']))
    print("val_binary_accuracy: " + str(history.history['val_binary_accuracy']))
    print("val_precision_metric: " + str(history.history['val_precision_metric']))
    print("val_recall_metric: " + str(history.history['val_recall_metric']))

    plt.figure()
    plt.plot(history.history['val_loss'])
    plt.plot(history.history['val_binary_accuracy'])
    plt.title("Validation loss and accuracy")
    plt.xlabel("Epoch")
    plt.legend(["validation loss", "validation accuracy"])
    plt.savefig("plots/val_lossvsacc.pdf")


if __name__ == '__main__':
    run()

