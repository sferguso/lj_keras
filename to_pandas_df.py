#!/usr/bin/env python3
import argparse
import numpy as np
from numpy import savetxt
from matplotlib import pyplot as plt
import h5py
import os
import json
import uproot
from keras.utils import to_categorical
from sklearn.model_selection import train_test_split
import pandas as pd

def get_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('input_file') #full path of input file
    parser.add_argument('--test',type=bool,default=False)
    parser.add_argument('--signal', type=bool, default=False)
    return parser.parse_args()

def run():
    print("start")
    args = get_args()
    print('input_file:test:signal ' + str(args.input_file) + ':' + str(args.test) + ':' + str(args.signal))

    ntotalevents = 100000
    if args.signal == True: 
        nevents=1000 #need to change manually below
        hdf5_filename_train = '/b/LJ_data/03Feb2020/pandas/eLJ-eLJ/sig-train-60k.hdf5'
        hdf5_filename_val = '/b/LJ_data/03Feb2020/pandas/eLJ-eLJ/sig-val-40k.hdf5'
        hdf5_key = 'sig'
    if args.signal == False: 
        nevents=100 #need to change manually
       	hdf5_filename_train = '/b/LJ_data/03Feb2020/pandas/eLJ-eLJ/bg-train-60k.hdf5'
       	hdf5_filename_val = '/b/LJ_data/03Feb2020/pandas/eLJ-eLJ/bg-val-40k.hdf5'
        hdf5_key = 'bg'
    print('nevents per file to be sampled: ' + str(nevents))

    print('output files: ')
    print(hdf5_filename_train)
    print(hdf5_filename_val)

    #open file with list of ntuples
    filepath_txt = args.input_file

    list_of_vars = [
        'part_LJindex','part_objNum','pdgID','nLJ7','nLJ6','nLJ5',
        'isMatched','PartToTrig', 'hasPV','trigMatch','trigxAODMatch',
        'part_pt','part_phi','part_eta',
        'el_f1','el_f3','el_f3core','el_e255','el_e277','el_weta1','el_weta2','el_fracs1','el_widths1',
        'el_widths2','el_poscs1','el_poscs2','el_asy1','el_pos','el_pos7','el_barys1','el_wtots1',
        'el_r33over37allcalo','el_ecore','el_Reta','el_Rphi','el_Eratio','el_Rhad','el_Rhad1','el_DeltaE',
        'calo_energyBE0','calo_energyBE1','calo_energyBE2','calo_energyBE3',
        'calo_rawE','calo_E', 
        'el_nTrackParticles',
        'el_deltaEta0','el_deltaEta1','el_deltaEta2','el_deltaEta3', 
        'el_deltaPhi0','el_deltaPhi1','el_deltaPhi2','el_deltaPhi3', 
        'el_deltaPhiRescaled0','el_deltaPhiRescaled1','el_deltaPhiRescaled2','el_deltaPhiRescaled3', 
        'el_deltaPhiFromLastMeasurement',
        'iso_ptcone20','iso_ptcone30','iso_ptcone40','iso_ptvarcone20','iso_ptvarcone30','iso_ptvarcone40',
        'iso_etcone20','iso_etcone30','iso_etcone40','iso_ettopocone20','iso_ettopocone30','iso_ettopocone40'
        ]

    vars_resize = ['LJ_pT','sumPtMu','sumPtEl']

    nfiles = 0
    nevents = 0
    data_df_all = pd.DataFrame()
    with open(filepath_txt) as f: 
        for i, line in enumerate(f):
            filepath = line[:-1]
            print('line number: ' + str(i) + ' filepath: ' + filepath)
            data = uproot.open(filepath)["analysis"]
            if args.test == True:
                print('numentries: ' + str(data.numentries))
            if data.numentries == 0: 
                #skip empty files
                print("file has zero events: " + filepath)
                continue
            data_df = data.pandas.df(list_of_vars, entrystop=5000) #change entrystop for signal/background
            if args.test == True:
                print("data_df shape: ")
                print(data_df.shape)
            #create an index to use to resize vars
            index_array = data.array('part_LJindex')
            index_array = index_array - 1 #make index start at 0
            #because LJ_pT has vector size equal to num of LJs, need to resize
            LJ_pT = data.array('LJ_pT')
            LJ_pTnew = LJ_pT[index_array].flatten()
            data_df['LJ_pT'] = LJ_pTnew[0:len(data_df)]
            if args.test == True:
                print("LJ_pT shape: ")
                print(data_df['LJ_pT'].shape)

            #make the index start at 0 instead of 1
            data_df['part_LJindex'] = data_df['part_LJindex'] - 1
            if args.test==True:
                print("with LJ_pT: " + str(data_df.head()))

            #make LJ selections
            #make trigMatch variable boolean from float
            data_df['trigMatch'] = data_df['trigMatch'].astype(bool)
            event_sel = data_df['hasPV'] & data_df['trigMatch']
            channel_sel = (data_df['nLJ7']>1) & (data_df['nLJ6'] == 0) & (data_df['nLJ5'] == 0)
            final_sel = event_sel & channel_sel
            data_df = data_df[final_sel]
            if args.test==True:
                print("after selection: " + str(data_df.head()))

            #make target variable
            if args.signal == False: 
                targets_bg = np.ones(data_df.shape[0])
                targets = to_categorical(targets_bg)
                data_df['targets_LJ'] = targets[:,0]
                if args.test==True: 
                    print('bg so targets_LJ should be 0: ' + str(data_df['targets_LJ']))
                data_df['targets_notLJ'] = targets[:,1]
            if args.signal == True:
                targets_sig = np.ones(data_df.shape[0])
                targets = to_categorical(targets_sig)
                data_df['targets_notLJ'] = targets[:,0]
                data_df['targets_LJ'] = targets[:,1]
                if args.test==True:	
                    print('sig so targets_LJ should be 1: ' + str(data_df['targets_LJ']))
            data_df_all = data_df_all.append(data_df, ignore_index=False)

    #sample only ntotalevents
    print('size of df before sampling: ' + str(data_df_all.shape))
    data_df_all = data_df_all.sample(min(len(data_df_all),ntotalevents))
    print('size of df: ' + str(data_df_all.shape))

    #create training and validation split
    train_df, val_df = train_test_split(data_df_all, test_size=0.3)
    print('size of training: ' + str(train_df.shape))
    print('size of validation: ' + str(val_df.shape))

    if args.test==True:
        print("training dataframe: " + str(train_df.head()))
        print("validation dataframe: " + str(val_df.head()))

    export_hdf5_train = train_df.to_hdf(hdf5_filename_train, hdf5_key, mode='a')
    export_hdf5_val = val_df.to_hdf(hdf5_filename_val, hdf5_key, mode='a')

    return

if __name__ == '__main__':
    run()


