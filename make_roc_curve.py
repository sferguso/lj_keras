#!/usr/bin/env python3

import uproot
import argparse
import numpy as np
from matplotlib import pyplot as plt
import h5py
import os

def get_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('input_file')
    parser.add_argument('-o','--output-dir', default='plots')
    parser.add_argument('-n', '--nn', nargs=2,
                        help='nn architecture and weights')
    return parser.parse_args()

# Start by defining the bounds of the histograms
BOUNDS = {
    'el_Eratio': (-0.2, 1.2),
    'el_f1': (-0.2, 1.2),
    'el_f3': (-0.2, 0.5),
    'el_f3core': (-0.2, 0.5),
    'nn': (-10, 15)
}

def run():
    args = get_args()

    analysis = uproot.open(args.input_file)["analysis"]
    
    #Lets make some arrays:
    mycache = {}
    # first time: reads from file
    # any other time: reads from cache
    DiscrVar = analysis.arrays(["el_Eratio","el_f1","el_f3","el_f3core"], cache=mycache);
    #print(DiscrVar)

    #set NaN values to small value
    DiscrVar[b'el_Eratio'][np.isnan(DiscrVar[b'el_Eratio'])] = -1000

    # make an axis to draw some distributinos
    ax = plt.subplot(1,1,1)
    for varname, (lowbin, highbin) in BOUNDS.items():

        #take the variable
        var = jets[varname]

        # set NaN values to small value (should only show up in rnnip_ratio)
        var[np.isnan(var)] = -9
        # define the bin bounds (note that we want overflow, thus the inf)
        edges = np.concatenate(
            [[-np.inf], np.linspace(lowbin, highbin, 500), [np.inf]])

        # now loop over signal and background to calculate efficiencies
        # for each working point
        all_eff = {}
        for mask, name in masks:
            discrim, _ = np.histogram(var[mask], bins=edges)
            # normalize the integrated discriminant note that we want
            # to start with high values, thus the [::-1] to flip the
            # axes.
            eff = np.cumsum(discrim[::-1])[::-1] / discrim.sum()
            all_eff[name] = eff

        # calculate efficinecy and background rejection
        light_eff = all_eff['light']
        # we don't want to divide by zero, so filter out points with
        # no background
        valid_rej = light_eff > 0
        valid_eff = (all_eff['b'] > 0.3) & (all_eff['b'] != 1.0)
        valid = valid_rej & valid_eff

        b_eff = all_eff['b'][valid]
        light_rejection = 1/light_eff[valid]

        # draw the plot
        ax.plot(b_eff, light_rejection, label=varname)

    # put things on the plot
    ax.set_yscale('log')
    ax.set_xlabel('b efficiency')
    ax.set_ylabel('light rejection')
    ax.legend()

    # make the output directory
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    plt.savefig('{}/roc.pdf'.format(output_dir))
    plt.close()

    n, bins, patches = plt.hist(DiscrVar[b'el_Eratio'].flatten(), 100, density = True)
    plt.title("Eratio")
    plt.ylabel("number of events")
    plt.xlim(xmin=0, xmax=1.5)
    plt.show()




if __name__ == '__main__':
    run()
