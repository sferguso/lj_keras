using Keras for LJ 

for use of all on fester begin with: 

    module load anaconda3

to_pandas_df.py: This is code that loops over a list of full path names (saved in a text file) of tuples and creates a
pandas data frame out of it. It requires one input: path to text file with full path names in it. 
You must first specify the number of total events you want to be in the final df, as well as how many events/file to use. 
After looping over all the files and appending the df into one, the df is randomized and a random selection of size specified is saved to hdf5

It has optional input: 
--is_bg = True/False (True by default)
To run: 

    ./to_pandas_df.py filesnames.tx

get_corr.py: This is code that takes the input dataframe and creates correlation plot and varable plots of signal and background
on the same figure. It requires three inputs: 

    1. input_file_sig is the list of full path names of signal files,
    2. input_file_bg is the list of full path names of background files, 
    3. variable type 

It also has optional inputs: 

    --LJ1_type = 5,6, or 7 (default 7, 5=mu, 6=mix, 7=e) 
    --LJ2_type = 5,6, or 7 (default 7, 5=mu, 6=mix, 7=e)

Plots are saved in a directory plots/ (that you first need to create)
To run: 

    ./get_corr.py path/to/sig.csv path/to/bg.csv shower_shape

train_nn.py: This is code that take the input of dataframe and trains and creates a machine learning nn. It is a working progress. 
It requires four inputs for full paths of files: these are (in this order): 
    
    signal_train.hdf5, bg_train.hdf5, signal_validation.hdf5, bg_validation.hdf5 

It also has optional inputs: 

    --LJ1_type = 5,6, or 7 (default	7, 5=mu, 6=mix, 7=e)
    --LJ2_type = 5,6, or 7 (default	7, 5=mu, 6=mix, 7=e)
    --epochs (or -e) = integer (this is the number of times the training loops over the data set, default = 1)
    --patience (or -p) = integer (this is the patience for the early stop - how many times it will keep trying when the loss is not getting better)
    --output-dir (or -o) = string (this is the path the model gets saved to. default = model)

To run: 

    ./train_nn.py path/to/sig_train.hdf5 path/to/bg_train.hdf5 path/to/sig_val.hdf5 path/to/bg_val.hdf5 -e=number -p=number


OLD: 

multinode.slurm: is the launcher job for it. Important to NOT do the max number of jobs per node (16). Below is 8 jobs per nodes. 

    #SBATCH -J Pandas
    #SBATCH -n 40 #Total number of tasks to run at one time
    #SBATCH -N 5 #Number of nodes on which to distribute the tasks evenly
    #SBATCH -p normal
    #SBATCH -o PandasJob.o%j
    #SBATCH -t 24:00:00