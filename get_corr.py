#!/usr/bin/env python3
import argparse
import numpy as np
import pandas as pd
import matplotlib
from matplotlib import pyplot as plt
import h5py
import os
import json
import uproot

def get_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('var_type') #options: shower_shape, track_cluster, isolation, calo_e,
    parser.add_argument('--input_file_sig', default = '/b/LJ_data/03Feb2020/pandas/FRVZ/FRVZ_all.csv')
    parser.add_argument('--input_file_bg', default = '/b/LJ_data/03Feb2020/pandas/databackground.csv')
    #LJ type: 7-eLJ, 6-mixLJ, 5-muLJ (order does not matter)
    parser.add_argument('--LJ1_type', type=int, default=7)
    parser.add_argument('--LJ2_type', type=int, default=7)
    parser.add_argument('-e','--epochs', type=int, default=1)
    parser.add_argument('-o','--output-dir', default='model')
    return parser.parse_args()

###############################################################
# Main Training Part
###############################################################

def run():
    print("start")
    args = get_args()

    if args.var_type == 'shower_shape':
        list_of_vars = [
            'nLJ7','nLJ6','nLJ5',
            'el_f1','el_f3','el_f3core','el_e255','el_e277','el_weta1','el_weta2','el_fracs1','el_widths1',
            'el_poscs1','el_poscs2','el_asy1','el_pos','el_pos7','el_barys1','el_wtots1',
            'el_r33over37allcalo','el_ecore','el_Reta','el_Rphi','el_Eratio','el_Rhad','el_Rhad1','el_DeltaE',
            'el_nTrackParticles',
            ]
        bounds = {'el_f1':(-.1,.8),'el_f3':(-.05,.4),'el_f3core':(-.5,.4),'el_e255':(0,300000),'el_e277':(0,300000),'el_weta1':(0,.9),
            'el_weta2':(-.05,.1),'el_fracs1':(-0.5,1.5),'el_widths1':(0.5,1.75),'el_poscs1':(-.6,.6),'el_poscs2':(0,1.1),
            'el_asy1':(-0.5,0.5),'el_pos':(-1,1),'el_pos7':(-5,5),'el_barys1':(-2.5,2.5),'el_wtots1':(0,10),'el_r33over37allcalo':(-.2,1.2),
            'el_ecore':(0,500000),'el_ecore':(0,300000),'el_Reta':(-0.2,1.5),'el_Rphi':(-0.2,1.5),'el_Eratio':(0,1.1),'el_Rhad':(-.2,2),
            'el_Rhad1':(-.2,2),'el_DeltaE':(0,2000),'el_nTrackParticles':(-1,20)}

    elif args.var_type == 'track_cluster':
        list_of_vars = [
            'nLJ7','nLJ6','nLJ5',
            'el_deltaEta0','el_deltaEta1','el_deltaEta2','el_deltaEta3',
            'el_deltaPhi0','el_deltaPhi1','el_deltaPhi2','el_deltaPhi3',
            'el_deltaPhiRescaled0','el_deltaPhiRescaled1','el_deltaPhiRescaled2','el_deltaPhiRescaled3',
            'el_deltaPhiFromLastMeasurement',
            ]
        bounds = {'el_deltaEta0':(-.3,.3),'el_deltaEta1':(-.3,.3),'el_deltaEta2':(-.3,.3),'el_deltaEta3':(-.3,.3),
            'el_deltaPhi0':(-1,1),'el_deltaPhi1':(-1,1),'el_deltaPhi2':(-1,1),'el_deltaPhi3':(-1,1),
            'el_deltaPhiRescaled0':(-1,1),'el_deltaPhiRescaled1':(-1,1),'el_deltaPhiRescaled2':(-1,1),'el_deltaPhiRescaled3':(-1,1),
            'el_deltaPhiFromLastMeasurement':(-1,1)}

    elif args.var_type == 'isolation':
        list_of_vars = [
            'nLJ7','nLJ6','nLJ5',
            'iso_ptcone20','iso_ptcone30','iso_ptcone40','iso_ptvarcone20','iso_ptvarcone30','iso_ptvarcone40',
            'iso_etcone20','iso_etcone30','iso_etcone40','iso_ettopocone20','iso_ettopocone30','iso_ettopocone40',
            ]
        bounds = {'iso_ptcone20':(0,300000),'iso_ptcone30':(0,300000),'iso_ptcone40':(0,300000),
            'iso_ptvarcone20':(0,300000),'iso_ptvarcone30':(0,300000),'iso_ptvarcone40':(0,300000),
            'iso_etcone20':(0,300000),'iso_etcone30':(0,300000),'iso_etcone40':(0,300000),
            'iso_ettopocone20':(0,300000),'iso_ettopocone30':(0,300000),'iso_ettopocone40':(0,300000)}

    elif args.var_type == 'calo_e':
        list_of_vars = [
            'nLJ7','nLJ6','nLJ5',
            'calo_energyBE0','calo_energyBE1','calo_energyBE2','calo_energyBE3',
            'calo_rawE','calo_E',
            ]
        bounds = {'calo_energyBE0':(0,200000),'calo_energyBE1':(0,200000),'calo_energyBE2':(0,200000),'calo_energyBE3':(0,200000),
            'calo_rawE':(0,400000),'calo_E':(0,400000)}

    else:
        print("----unknown var_type argument----")


    #open signal file
    sig = pd.read_csv(args.input_file_sig)
    print("signal file read in.")

    #LJ type selection. create bool array for signal.
    #eLJ eLJ
    if args.LJ1_type == 7 and args.LJ2_type == 7:
        b_sel_sig = (sig["nLJ7"] > 1)
    #muLJ muLJ
    elif args.LJ1_type == 5 and args.LJ2_type == 5:
        b_sel_sig = (sig["nLJ5"] > 1)
    #eLJ muLJ
    elif (args.LJ1_type == 5 and args.LJ2_type == 7) or (args.LJ1_type == 7 and args.LJ2_type == 5):
        b_sel_sig = (sig["nLJ5"] > 0) and (sig["nLJ7"] > 0)
    #eLJ mixLJ
    elif (args.LJ1_type == 7 and args.LJ2_type == 6) or (args.LJ1_type == 6 and args.LJ2_type == 7):
        b_sel_sig = (sig["nLJ6"] > 0) and (sig["nLJ7"] > 0)
    #muLJ mixLJ
    elif (args.LJ1_type == 5 and args.LJ2_type == 6) or (args.LJ1_type == 6 and args.LJ2_type == 5):
        b_sel_sig = (sig["nLJ5"] > 0) and (sig["nLJ6"] > 0)
    #mixLJ mixLJ
    elif args.LJ1_type ==6 and args.LJ2_type == 6:
        b_sel_sig = (sig["nLJ6"] > 1)
    else:
        print("!!!!invalid LJ type!!!!")
    print("signal LJ selection done.")

    #apply selection to signal
    sig = sig[b_sel_sig]

    #open background file
    bg = pd.read_csv(args.input_file_bg, nrows=5000000)
    print("background file read in.")

    #LJ type selection. create bool array for background.
    #eLJ eLJ
    if args.LJ1_type == 7 and args.LJ2_type == 7:
        b_sel_bg = (bg["nLJ7"] > 1)
    #muLJ muLJ
    elif args.LJ1_type == 5 and args.LJ2_type == 5:
        b_sel_bg = (bg["nLJ5"] > 1)
    #eLJ muLJ
    elif (args.LJ1_type == 5 and args.LJ2_type == 7) or (args.LJ1_type == 7 and args.LJ2_type == 5):
        b_sel_bg = (bg["nLJ5"] > 0) and (bg["nLJ7"] > 0)
    #eLJ mixLJ
    elif (args.LJ1_type == 7 and args.LJ2_type == 6) or (args.LJ1_type == 6 and args.LJ2_type == 7):
        b_sel_bg = (bg["nLJ6"] > 0) and (bg["nLJ7"] > 0)
    #muLJ mixLJ
    elif (args.LJ1_type == 5 and args.LJ2_type == 6) or (args.LJ1_type == 6 and args.LJ2_type == 5):
        b_sel_bg = (bg["nLJ5"] > 0) and (bg["nLJ6"] > 0)
    #mixLJ mixLJ
    elif args.LJ1_type ==6 and args.LJ2_type == 6:
        b_sel_bg = (bg["nLJ6"] > 1)
    else:
        print("!!!!invalid LJ type!!!!")
    print("background LJ selection done.")

    #apply selection to bg
    bg = bg[b_sel_bg]

    #get correlation between columns/variables
    #first get rid of LJ type vars
    list_of_vars.remove('nLJ7')
    list_of_vars.remove('nLJ6') 
    list_of_vars.remove('nLJ5') 

    corr_sig = sig[list_of_vars].corr()
    corr_bg = bg[list_of_vars].corr()

    print("signal corr: " + str(corr_sig))
    print("background corr: " + str(corr_bg))

    #scatter plots of vars correlation
#    from pandas.plotting import scatter_matrix
#    scatter_matrix(sig, figsize=(6, 6))
#    plt.title("correlation of variables (MC signal)")
#    plt. show()

#    scatter_matrix(bg, figsize=(6, 6))
#    plt.title("correlation of variables (10% data background)")
#    plt. show()

    #plot correlation matrix for signal
    fig, ax = plt.subplots(figsize=(10,10))
    im, _ = heatmap(corr_sig, list_of_vars, list_of_vars,
                cmap="PuOr", vmin=-1, vmax=1,
                cbarlabel="correlation coeff.")

    def func(x, pos):
        return "{:.2f}".format(x).replace("0.", ".").replace("1.00", "")

    annotate_heatmap(im, valfmt=matplotlib.ticker.FuncFormatter(func), size=7)
    plt.tight_layout()
    plt.title("correlation matrix (MC signal)")
#    plt.show()
    name = 'plots/sig_corr_' + args.var_type + '.png'
    plt.savefig(name)
    plt.clf()
    
    #plot correlation matrix for background
    fig, ax = plt.subplots(figsize=(10,10))
    im, _ = heatmap(corr_bg, list_of_vars, list_of_vars,
                cmap="PuOr", vmin=-1, vmax=1,
                cbarlabel="correlation coeff.")

    def func(x, pos):
        return "{:.2f}".format(x).replace("0.", ".").replace("1.00", "")

    annotate_heatmap(im, valfmt=matplotlib.ticker.FuncFormatter(func), size=7)
    plt.tight_layout()
    plt.title("correlation matrix (10% data background)")
#    plt.show()
    name = 'plots/bg_corr_' + args.var_type + '.png'
    plt.savefig(name)
    plt.clf()

    #plot each variable with both sig and background. 
    n_bins = 100
    for var, (min, max)  in bounds.items(): 
        print("making var plot: " + var)
        fig, ax = plt.subplots(figsize=(8, 4))
        ax.hist([sig[var],bg[var]], n_bins, range=(min,max), label=['signal','background'], normed=True)
        ax.legend(prop={'size': 10})
        ax.set_title(var)
        fig.tight_layout()
        name = 'plots/sig_bg_' + args.var_type + '_' + var + '.png'
        plt.savefig(name)
        plt.close()

#heat map code copied from: https://matplotlib.org/gallery/images_contours_and_fields/image_annotated_heatmap.html#
def heatmap(data, row_labels, col_labels, ax=None,
            cbar_kw={}, cbarlabel="", **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Parameters
    ----------
    data
        A 2D numpy array of shape (N, M).
    row_labels
        A list or array of length N with the labels for the rows.
    col_labels
        A list or array of length M with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False,
                   labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=-30, ha="right",
             rotation_mode="anchor")

    # Turn spines off and create white grid.
    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar

def annotate_heatmap(im, data=None, valfmt="{x:.2f}",
                     textcolors=["black", "gray"],
                     threshold=None, **textkw):
    """
    A function to annotate a heatmap.

    Parameters
    ----------
    im
        The AxesImage to be labeled.
    data
        Data used to annotate.  If None, the image's data is used.  Optional.
    valfmt
        The format of the annotations inside the heatmap.  This should either
        use the string format method, e.g. "$ {x:.2f}", or be a
        `matplotlib.ticker.Formatter`.  Optional.
    textcolors
        A list or array of two color specifications.  The first is used for
        values below a threshold, the second for those above.  Optional.
    threshold
        Value in data units according to which the colors from textcolors are
        applied.  If None (the default) uses the middle of the colormap as
        separation.  Optional.
    **kwargs
        All other arguments are forwarded to each call to `text` used to create
        the text labels.
    """

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = matplotlib.ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[int(im.norm(data[i, j]) > threshold)])
            text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            texts.append(text)

    return texts

if __name__ == '__main__':
    run()
